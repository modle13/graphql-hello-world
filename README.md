2023-05-04 - GraphQL hello world

# insomnia docs

https://docs.insomnia.rest/insomnia/graphql-queries

# python guide

https://www.apollographql.com/blog/graphql/python/complete-api-guide/

## gotchas with python guide
### "Adding a database" section

- had to install the following (not included in the guide):
	- deps:
		- libpq-dev (ubuntu) (dependency for psycopg2)
		- psycopg2
	- https://stackoverflow.com/questions/11618898/pg-config-executable-not-found
- the url copied from elephantsql isn't valid for flask_sqlalchemy, change `postgres://` to `postgresql://`
	- https://stackoverflow.com/questions/62688256/sqlalchemy-exc-nosuchmoduleerror-cant-load-plugin-sqlalchemy-dialectspostgre

### "Creating a model" section

#### The provided create commands are insufficient:

```
>>> from app import db
>>> db.create_all()
```

https://stackoverflow.com/questions/31444036/runtimeerror-working-outside-of-application-context

Do this instead
```
>>> from app import db
>>> from app import app
>>> with app.app_context():
...     db.create_all()
... 
```

with `app.app_context():` is needed for any `db...` call

#### Wiring up Flask server and GraphQL with Ariadne library

error: `ImportError: cannot import name 'PLAYGROUND_HTML' from 'ariadne.constants'`

https://stackoverflow.com/questions/62387146/python-ariadne-graphql-cannot-import-name-graphqlnamedtype

solution:
```pip install "graphql-core<3.1"```

new error: `ModuleNotFoundError: No module named 'graphql.pyutils.undefined'`

https://stackoverflow.com/questions/11618898/pg-config-executable-not-found

solution: downgrade to ariadne: 0.17

`pip install ariadne==0.17`


#### Writing our first Resolver 

sample query 'posts' needs to be 'post'

## other notes

- queries can be executed in elephantsql.com's 'BROWSER' screen

